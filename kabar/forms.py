from . import models
from django import forms

class Status_Form(forms.ModelForm):
    message = forms.CharField(widget = forms.TextInput(attrs={
    	"placeholder": "Apa kabarmu?", 
    	"class":"input",
    	"required" : True,
    	"max_length" : 300,
    	}))

    class Meta:
    	model = models.Status
    	fields = ["message"]
