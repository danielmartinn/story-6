from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from . import views
from .models import Status
from .forms import Status_Form

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.
class UnitTest(TestCase):
	def test_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_calling_right_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.status)

	def test_template_exist(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_template_contain_status(self):
		self.assertIsNotNone(views.status)

	def test_model_created(self):
		Status.objects.create(message = "Halo, apa kabar?")
		status_objects = Status.objects.all()
		self.assertEqual(len(status_objects), 1)

		status_object = status_objects[0]
		self.assertEqual(status_object.message, "Halo, apa kabar?")
		self.assertEqual(status_object.time_message.minute, timezone.now().minute)
		self.assertEqual(status_object.time_message.hour, timezone.now().hour)
		self.assertEqual(status_object.time_message.date(), timezone.now().date())

class functional_test(LiveServerTestCase):
	def setUp(self):
	    chrome_options = Options()
	    chrome_options.add_argument('--dns-prefetch-disable')
	    chrome_options.add_argument('--no-sandbox')
	    chrome_options.add_argument('--headless')
	    chrome_options.add_argument('--disable-dev-shm-usage')
	    chrome_options.add_argument('disable-gpu')
	    self.selenium  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
	    super(functional_test, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(functional_test, self).tearDown()

	def test_initial_view(self):
	    selenium = self.selenium
	    selenium.get(self.live_server_url)
	    title = selenium.find_element_by_id('judul')

	def test_input(self):
		selenium = self.selenium
		self.selenium.get(self.live_server_url)
		# find the form element
		form_input = self.selenium.find_element_by_id('id_message')
		submit = self.selenium.find_element_by_id('submit')

		# fill the form input
		form_input.send_keys('coba')

		# submit
		submit.send_keys(Keys.RETURN)
		# check
		self.assertIn('coba', self.selenium.page_source)

		time.sleep(5)