from django.db import models
from django.utils import timezone

# Create your models here.

class Status(models.Model):
	message = models.CharField(max_length = 300)
	time_message = models.DateTimeField(default=timezone.now)
