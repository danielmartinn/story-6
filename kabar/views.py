from django.shortcuts import render,redirect
from .forms import Status_Form
from .models import Status

# Create your views here.
def status(request):
	form = Status_Form()
	status_list = Status.objects.order_by("-time_message")
	if request.method == "POST":
		form = Status_Form(request.POST)
		if form.is_valid():
			form.save()
			form = Status_Form()
			redirect("kabar:status")
	return render(request, 'index.html', {'form' : form, 'status_list' : status_list})