from django.urls import path, include
from . import views

app_name = 'kabar'

urlpatterns = [
    path('', views.status, name= "status"),
]
